let getDataGiphy=(url)=>{
    return new Promise(function(res,rej){
      var http= new XMLHttpRequest();
      http.open("GET",url,true);
      http.onload=function(){
        if(http.status==200){
          res(JSON.parse(http.response));
        }else{
          rej(http.status)
        }
      }
      http.onerror = function(){
        rej(http.status);
      }
      http.send();
    });
  }
  let createVariable=()=>{
    let inputData = document.querySelector('input');
    const section = document.querySelector('.one');
    return {
      input:inputData,
      section:section
    }
  }
  let search =()=>{
    let funInstant = createVariable();
    let section = funInstant.section;
    clear(section);
    let input = funInstant.input;
    getDataGiphy(`https://api.giphy.com/v1/gifs/search?q=${input.value}&limit=32&api_key=dc6zaTOxFJmzC`).then(function(data){
      let dane =data.data;
      if(dane.length == 0){
        imageError(section);
      }else{
        doImages(dane,section);
      }
    });
    input.value="";
  }
  let clear=(element)=>{
      while(element.firstChild){
        element.removeChild(element.firstChild)
      }
  }
  let imageError=(section)=>{
    let error = document.createElement('h2');
    error.innerHTML = "brak wyników";
    error.setAttribute('class','error');
    section.appendChild(error);
  }
  let doImages=(data,section)=>{
    data.forEach(function(x){
      let img= document.createElement('img');
      img.setAttribute('src',x.images.downsized.url);
      section.appendChild(img);
    });
  }
  window.addEventListener('keyup',function(e){
    if(e.keyCode == 13) search()
  });
